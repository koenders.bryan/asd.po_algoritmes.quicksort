﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Quicksort.Tests
{
    public class Tests
    {
        [Fact]
        public void Sorting_An_Unsorted_List_Equals_Sorted_List_Should_Be_True()
        {
            //Arrange
            List<int> sortedList = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            List<int> unsortedList = new List<int> { 2, 5, 9, 7, 10, 4, 3, 1, 6, 8 };

            //Act
            Quicksort.Sort(unsortedList);

            //Assert
            Assert.True(Enumerable.SequenceEqual(unsortedList, sortedList));
        }

        [Fact]
        public void Sorting_An_Unsorted_List_Containing_Negative_Values_Equals_Sorted_List_Should_Be_True()
        {
            //Arrange
            List<int> sortedList = new List<int> { -3, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            List<int> unsortedList = new List<int> { 2, 5, 9, 7, 10, 4, -3, 3, 1, 6, 8 };

            //Act
            Quicksort.Sort(unsortedList);

            //Assert
            Assert.True(Enumerable.SequenceEqual(unsortedList, sortedList));
        }

        [Fact]
        public void Sorting_An_List_Containing_The_Same_Numbers_Should_Succeed()
        {
            //Arrange
            List<int> sortedList = new List<int> { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
            List<int> unsortedList = new List<int> { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };

            //Act
            Quicksort.Sort(unsortedList);

            //Assert
            Assert.True(Enumerable.SequenceEqual(unsortedList, sortedList));
        }

        [Fact]
        public void Sorting_A_Sorted_List_Should_Succeed()
        {
            //Arrange
            List<int> sortedList = new List<int> { 2, 23, 34, 44, 45, 56, 97, 658 };

            //Act
            Quicksort.Sort(sortedList);

            //Assert
            Assert.True(Enumerable.SequenceEqual(sortedList, sortedList));
        }

        [Fact]
        public void Sorting_A_Null_Reference_Should_Throw_A_Null_Reference_Exception()
        {
            //Arrange
            List<int> sortedList = new List<int> { 2, 23, 34, 44, 45, 56, 97, 658 };
            List<int> unsortedList = null;

            //Assert
            Assert.Throws<NullReferenceException>(() => Quicksort.Sort(unsortedList));
        }

        [Fact]
        public void Sorting_An_Unsorted_List_Of_IComparable_Derived_Objects_Equals_Sorted_List_Should_Be_True()
        {
            //Arrange
            List<Car> sortedList = new List<Car> {
                new Car() {
                    Brand = "Volkswagen",
                    Model = "Golf IV",
                    ModelYear = 1997
                },
                new Car() {
                    Brand = "Volkswagen",
                    Model = "Golf V",
                    ModelYear = 2004
                },
                new Car() {
                    Brand = "Volkswagen",
                    Model = "Golf VI",
                    ModelYear = 2009
                },
                new Car() {
                    Brand = "Volkswagen",
                    Model = "Golf VII",
                    ModelYear = 2014
                },
                new Car() {
                    Brand = "Volkswagen",
                    Model = "Golf VIII",
                    ModelYear = 2020
                }
            };

            List<Car> unsortedList = new List<Car> {
                sortedList[1],
                sortedList[4],
                sortedList[0],
                sortedList[3],
                sortedList[2]
            };

            //Act
            Quicksort.Sort(unsortedList);

            //Assert
            Assert.True(Enumerable.SequenceEqual(unsortedList, sortedList));
        }

        [Theory]        
        [InlineData(new int[] { 2, 5, 12, 23, 33, 46, 75, 87, 91 }, 33)] //Sorted uneven collection        
        [InlineData(new int[] { 5, 87, 23, 46, 91, 2, 12, 33, 75 }, 75)] //Unsorted uneven collection
        [InlineData(new int[] { 2, 5, 12, 23, 33, 46, 75, 87 }, 33)] //Sorted even collection
        [InlineData(new int[] { 5, 87, 23, 46, 2, 12, 33, 75 }, 5)] //Unsorted even collection
        public void Median_Of_Three_Pivot_Selection_Should_Return_Median(int[] collection, int median)
        {
            Assert.Equal(Quicksort.SelectPivot(collection), median);
        }
    }
}