using System;
using System.Collections.Generic;
using System.Linq;

namespace Quicksort
{
    public static class Quicksort
    {
        public static void Sort<T>(List<T> collection) where T : IComparable<T>
        {
            if (collection == null)
            {
                throw new NullReferenceException("Collection cannot be null");
            }

            //Upon having 1 item or no items, the collection is already sorted. We break the method.
            if (collection.Count <= 1)
            {
                return;
            }                

            //Select pivot
            var pivot = SelectPivot(collection);

            //Define new lists for less, equal and greater than the pivot.
            List<T> less = new List<T>(),
                    equal = new List<T>(),
                    greater = new List<T>();

            //Iterate over collection and add item to the less, equal or greater collection based on comparison result.
            foreach(var item in collection)
            {
                //-1 = smaller, 0 = equals, 1 = larger.
                var comparisonResult = item.CompareTo(pivot);

                switch (comparisonResult)
                {
                    case -1:
                        less.Add(item);
                        break;
                    case 0:
                        equal.Add(item);
                        break;
                    case 1:
                        greater.Add(item);
                        break;
                }
            }

            //Keep recursively sorting until the collections are fully sorted
            Sort(less);
            Sort(greater);

            //Merge the sorted subcollections
            collection.Clear();
            collection.AddRange(less);
            collection.AddRange(equal);
            collection.AddRange(greater);
        }

        public static T SelectPivot<T>(IEnumerable<T> collection) where T : IComparable<T>
        {
            // Median of three
            var first = collection.First();
            var middle = collection.ElementAt(collection.Count() / 2);
            var last = collection.Last();

            //Add up comparison results to find the smallest and largest values
            //2 means larger than both
            //-2 means smaller than both
            //0 means smaller and larger than the others
            var firstComparison = first.CompareTo(middle) + first.CompareTo(last);
            var middleComparison = middle.CompareTo(first) + middle.CompareTo(last);
            var lastComparison = last.CompareTo(first) + last.CompareTo(middle);

            //first is greater than middle and middle is greater than last, first is greater than both
            //first is smaller than middle and middle is smaller than list, last is greater than both
            if (
                firstComparison > middleComparison && middleComparison > lastComparison || 
                firstComparison < middleComparison && middleComparison < lastComparison
            )
            {
                return middle;
            }        
            //middle is greater than last and last is greater than first, middle is greater than both
            //middle is smaller than last and last is smaller than first, first is greater than both
            else if (
                middleComparison > lastComparison && lastComparison > firstComparison || 
                middleComparison < lastComparison && lastComparison < firstComparison
            )
            {
                return last;
            }
            //last is greater than first and first is greater than middle, last is greater than both
            //last is smaller than first and first is smaller than middle, middle is greater than both
            else if (
                lastComparison > firstComparison && firstComparison > middleComparison || 
                lastComparison < firstComparison && firstComparison < middleComparison
            )
            {
                return first;
            }
                
            return first;
        }
    }
}