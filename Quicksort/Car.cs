using System;

namespace Quicksort
{
    public class Car : IComparable<Car>
    {
        public string Brand { get; set; }

        public string Model { get; set; }

        public int ModelYear { get; set; }

        public int CompareTo(Car other)
        {
            if (ModelYear < other.ModelYear)
            {
                return -1;
            }                
            else if (ModelYear > other.ModelYear)
            {
                return 1;
            }
               
            return 0;
        }
    }
}