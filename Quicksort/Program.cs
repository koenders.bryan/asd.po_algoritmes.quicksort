﻿using System;
using System.Collections.Generic;

namespace Quicksort
{
    class Program
    {
        static void Main(string[] args)
        {
            //Generate random list.
            var list = new List<int>();
            var randNum = new Random();
            var c = 10;

            for (int i = 0; i < c; i++)
            {
                list.Add(randNum.Next(0, c));
            }

            Quicksort.Sort(list);
            Console.ReadLine();
        }
    }
}
